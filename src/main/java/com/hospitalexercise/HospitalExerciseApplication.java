package com.hospitalexercise;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HospitalExerciseApplication {

    public static void main(String[] args) {
        SpringApplication.run(HospitalExerciseApplication.class, args);
    }

}
